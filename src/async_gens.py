# David Beazley
# 2015 PyCon

import socket
from select import select

tasks = []  # список генераторов

to_read = {}  # сокет : генератор
to_write = {}


def server():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # AF_INET: ipv4 SOCK_STREAM: tcp
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  # переиспользование адреса
    server_socket.bind(('localhost', 5000))  # адрес
    server_socket.listen()  # включение сокета
    while True:
        yield ('read', server_socket)
        client_socket, addr = server_socket.accept()  # сервер-сокет становится ready_to_read только когда
        # есть подключения для обработки

        print('Connection from', addr)

        tasks.append(client(client_socket))


def client(client_socket: socket.socket):
    while True:
        yield ('read', client_socket)
        request = client_socket.recv(4096)  # становится ready_to_read только когда есть сообщение
        if not request:
            break
        else:
            response = 'Hello world\n'.encode()
            yield ('write', client_socket)  # становится ready_to_write только когда есть сообщение для клиента
            client_socket.send(response)


def event_loop():
    while any([tasks, to_read, to_write]):
        # По сути конец роунд робина
        while not tasks:
            # в тот момент когда все сокеты и генераторы распределены по спискам на чтение и запись
            # мы можем проверить готовы ли сокеты для таких операций
            # те, кто готов отправляются в очередь задач, где контроль выполнения отдается обратно генератору
            ready_to_read, ready_to_write, _ = select(to_read, to_write, [])
            for sock in ready_to_read:
                tasks.append(to_read.pop(sock))
            for sock in ready_to_write:
                tasks.append(to_write.pop(sock))
        # По сути начало роунд робина
        try:
            task = tasks.pop(0)  # берем первый генератор из списка и удаляем его
            reason, sock = next(task)  # выполняем задачу. в нашем случае задача будет представлять
            # из себя фрагмент выполнения кода генератора до следующего yield включительно

            # после выполнения задачи yield говорит какое действие будет следующее по списку
            # добавляем  в соответстующий список
            if reason == 'read':
                to_read[sock] = task  # добавляем генератор в соответствующий список
            if reason == 'write':
                to_write[sock] = task
        except StopIteration:
            print('done')


tasks.append(server())
event_loop()
