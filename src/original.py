import socket

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # AF_INET: ipv4 SOCK_STREAM: tcp
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  # переиспользование адреса
server_socket.bind(('localhost', 5000))  # адрес
server_socket.listen()  # включение сокета

while True:
    print('Before .accept()')
    client_socket, addr = server_socket.accept()  # возвращает tuple(сокет клиента, адрес), блокирующая операция
    print('Connection from', addr)

    while True:
        print('Before .recv()')
        request = client_socket.recv(4096)
        if not request:
            break
        else:
            response = 'Hello world\n'.encode()
            client_socket.send(response)  # если буфер отправки полон тоже блокирующая операция
