import requests
from time import time


def get_file(url):
    # используемый url работает с переадресацией, поэтому нужно проставить allow_redirects
    response = requests.get(url, allow_redirects=True)
    return response


def write_file(response: requests.Response):
    # делим юрл с которого получили файл по / и берем последний элемент массива, который
    # и является названием файла
    filename = response.url.split('/')[-1]
    with open(f"data/{filename}", 'wb') as file:
        file.write(response.content)


def main():
    url = "https://loremflickr.com/320/240"
    t0 = time()
    for i in range(10):
        write_file(get_file(url))
    print(time() - t0)


if __name__ == "__main__":
    main()
