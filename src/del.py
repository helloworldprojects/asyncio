# Делегирующий генератор - тот генератор, который вызывает какой-нибудь другой
# Подгенератор - вызываемый другим генератором
def coroutine(func):  # декоратор необходим для того, чтобы запускать выполнение генератора сразу без g.send(None)/next(g)
    # !!! не определяет, что это корутина, это просто для удобства их запуска !!!
    def inner(*args, **kwargs):
        g = func(*args, **kwargs)
        g.send(None)
        return g
    return inner


class SomeException(Exception):
    pass


@coroutine
def subgen():  # простая корутина / подгенератор
    while True:
        try:
            message = yield
        except SomeException:
            print('Some exception catched')
        else:
            print("...", message)


@coroutine
def delegator(g):
    while True:
        try:
            data = yield  # получаем на вход данные send и throw
            g.send(data)  # перенаправляем данные send
        except SomeException as e:  # перенаправляем SomeException в подгенератор
            g.throw(e)


def subgen_from():  # так как yield from содержит в себе инициализацию подгенератора, декоратор можно убрать
    while True:
        try:
            message = yield
        except StopIteration:
            print('StopIteratioin catched, stop')
            break
        except SomeException:
            print('SomeException catched')
        else:
            print("...", message)
    return "Return subgen"


@coroutine
def delegator_from(g):  # по сути вся конструкция delegator заменена конструкцией yield from g
    result = yield from g  # делегирующий генератор раскрывается в ретерне, суть в том
    print(result)
    # что в данном случае мы не пишем какие либо tryexcept а просто получаем результат, то есть
    # вот эта конструкция уже не нужна:
    # g = subgen_from()
    # try:
    #     g.throw(SomeException)
    # except SomeException as e:
    #     print(e.value)


def test():
    sg = subgen_from()
    d = delegator_from(sg)
    d.send("mes1")
    d.send("mes2")
    d.send("mes3")
    d.throw(SomeException)
    d.throw(StopIteration)


test()

# примечание: по сути конструкция for in вызывает функцию next у генератора,
# то есть по сути генератор может быть итерируемым объектом

# то, что заканчивается все вылетом со StopIteration обусловленно тем, что мы снаружи его нигде не ловим,
# но вообще говоря все отрабатывает

# yield from аналог await в других языках
# по сути когда вызывается такая конструкция, она блокирует делегирующий генератор и заставляет его ждать
# когда закончит работу подгенератор. Таким образом подгенератор должен содержать завершающую стадию,
# иначе делигатор будет заблокирован навсегда

# наверняка
# client_socket, addr = server_socket.accept() содержит в себе код, который бесконечно ходит по кругу и ждет пока появится подключение
# когда оно появляется - он возвращает новый сокет и завершает работу.
