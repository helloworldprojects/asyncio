import asyncio  # есть в станадартных библиотеках python с 2014 (3.4+)


@asyncio.coroutine
def print_nums():
    num = 1
    while True:
        print(num)
        num += 1
        # time.sleep(1) блокирующая, так что не подходит, поток выполнения нельзя блокировать
        yield from asyncio.sleep(1)  # решает проблему, когда происходит задержка - контроль выполнения
        # передается обратно в событийный цикл, сама же функция будет представлять из себя подгенератор
        # поэтому необходимо использовать yield from


@asyncio.coroutine
def print_time():
    count = 0
    while True:
        if count % 3 == 0:
            print(f"{count} seconds have passed")
        count += 1
        yield from asyncio.sleep(1)


@asyncio.coroutine
def main():
    task1 = asyncio.ensure_future(print_time())  # сам тип данных task - это наследник Future, в других
    task2 = asyncio.ensure_future(print_nums())  # !!! скобки, корутина - это генератор !!!
    # языках Promise, он обещает, что задача будет выполнена позже
    #
    # при запуске можно увидеть, что задачи работают асинхронно
    yield from asyncio.gather(task1, task2)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()  # создание событийного цикла
    # запускаем цикл до тех пор пока не закончится с корутиной main (делигирующий генератор)
    loop.run_until_complete(main())
    loop.close()
# В примере ниже показано, что asyncio.coroutine делает из функции генератор
# >>> import asyncio
# >>> @asyncio.coroutine
# ... def a():
# ...     return "ok"
# ...
# >>> import inspect
# >>> inspect.isgeneratorfunction(a)
# True
# >>> g = a()
# >>> next(g)
# Traceback (most recent call last):
#   File "<stdin>", line 1, in <module>
# в объекте исключения содержится return
# StopIteration: ok
