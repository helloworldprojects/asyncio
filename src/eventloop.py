import socket
from select import select

to_monitor = []

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # AF_INET: ipv4 SOCK_STREAM: tcp
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  # переиспользование адреса
server_socket.bind(('localhost', 5000))  # адрес
server_socket.listen()  # включение сокета


def accept_connection(server_socket: socket):
    client_socket, addr = server_socket.accept()  # возвращает tuple(сокет клиента, адрес), блокирующая операция
    print('Connection from', addr)
    to_monitor.append(client_socket)  # добавляем сокет в список для мониторинга,
    # чтобы когда в него можно было писать
    # мы туда отправили сообщение


def send_message(client_socket: socket):
    request = client_socket.recv(4096)  # принимаем сообщение от сокета
    if request:  # если оно не пустое - отправляем сообщение
        response = 'Hello world\n'.encode()  # сообщение должно быть закодировано
        client_socket.send(response)  # если буфер отправки полон тоже блокирующая операция
    else:  # если сообщение пустое нужно закрыть соединение и убрать сокет из списка мониторинга
        to_monitor.remove(client_socket)
        client_socket.close()


def event_loop():
    while True:
        ready_to_read, _, _ = select(to_monitor, [], [])  # read, write, errors списки файлов

        for sock in ready_to_read:  # для каждого сокета в который уже можно писать
            if sock is server_socket:  # если это серверный сокет можем принять новое подключение
                accept_connection(sock)
            else:  # если это клиентский сокет можем отправить сообщение
                send_message(sock)


if __name__ == "__main__":
    to_monitor.append(server_socket)  # добавляем серверны сокет в список сокетов для мониторинга
    # чтобы когда можно было принять подключение мы его приняли
    event_loop()
