import asyncio
from time import time
import aiohttp


def write_file(data, url):  # асинхронная запись возможна только с помощью нескольких потоков
    # такую функцию может предоставить aiofiles, здесь мы ее не рассматриваем
    # поэтому это синхронная функция
    filename = f"data/asyncio-{str(url).split('/')[-1]}.jpg"
    with open(filename, 'wb') as file:
        file.write(data)


async def fetch_content(url, session):
    # асинхронный контекст здесь все объекты - это генераторы-корутины
    # поэтому вместо того, чтобы использовать response = session.get(url, allow_redirects=True)
    # мы должны использовать конструкцию async with
    async with session.get(url, allow_redirects=True) as response:
        data = await response.read()  # получаем
        write_file(data, response.url)  # синхронная функция
        # на самом деле хорошей практикой является использование только асинхронного кода
        # без перемешивания с синхронным, но здесь в принципе можно


async def main():
    url = "https://loremflickr.com/320/240"
    tasks = []
    async with aiohttp.ClientSession() as session:  # контекстный менеджер с сессией, которая позволяет делать http-запросы
        for i in range(10):  # создаем 10 асинхронных задач, которые делают одно и тоже
            task = asyncio.create_task(fetch_content(url, session))
            tasks.append(task)

        await asyncio.gather(*tasks)


if __name__ == "__main__":
    t0 = time()
    asyncio.run(main())
    print(time() - t0)

# по времени произошла оптимизация в 6 раз
