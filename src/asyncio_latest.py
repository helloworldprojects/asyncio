# в python 3.5 появилась команда async
import asyncio  # есть в станадартных библиотеках python с 2014 (3.4+)


async def print_nums():  # декоратор для создания корутины в 3.5 заменен на async
    num = 1
    while True:
        print(num)
        num += 1
        # декоратор для ожидания окончания работы асинхронной функции заменен с yield from на await
        await asyncio.sleep(1)


async def print_time():
    count = 0
    while True:
        if count % 3 == 0:
            print(f"{count} seconds have passed")
        count += 1
        await asyncio.sleep(1)


async def main():
    task1 = asyncio.create_task(print_time())  # создание задачи заменено на create_task
    task2 = asyncio.create_task(print_nums())

    await asyncio.gather(task1, task2)  # собирает результаты работы функций,
    # если задачи бесконечны, то никогда не сработает


if __name__ == "__main__":
    # c python3.7 данную конструкцию можно заменить одной строкой
    # loop = asyncio.get_event_loop()
    #
    # loop.run_until_complete(main())
    # loop.close()
    asyncio.run(main())
