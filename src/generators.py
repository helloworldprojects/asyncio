from time import time

# реализуется принцип RoundRobin


def gen(string_object: str):  # при вызове next(объект_генератора) будет выдаваться буква строки
    # до тех пор пока она не кончится, дальше exception StopIteration, так как следубщего yield нету
    for char in string_object:
        yield char


def gen_filename():  # при вызове next(объект_генератора) будет постоянно выдаваться новая строка
    while True:
        pattern = 'file-{}.jpg'
        t = int(time() * 1000)  # таймстемп

        yield pattern.format(str(t))
        # !!!функция next(объект_генератора) сдвигает выполнение генератора до следующего
        # применения функции yield!!!
        # то есть при первом вызове он не сделает например что написано после yield
        # но во второй вызов уже надо будет пройти дальше по коду, а там еще дополнительная часть
        # которую надо пройти, чтобы выйти на следующую итерацию
        sum = 123 + 2456
        print(sum)
        # соответственно yield может быть в разных частях кода


def gen2(n):
    for num in range(n):
        yield num


g = gen("bc")
g2 = gen2(4)
tasks = [g, g2]

# Round Robin реализация, генераторы по очереди срабатывают до тех пор
# пока не будут получать StopIteration на каждой итерации от обоих генераторов
# (в этот момент список задач будет пуст)
while tasks:
    task = tasks.pop(0)
    try:
        i = next(task)
        print(i)
        tasks.append(task)
    except StopIteration:
        pass

# при взаимодействии с генератором (yield вместо return) его можно прервывать в отличии
# от функции, что можно увидеть ниже
# python3 -i generators.py
#
# >>> next(g)
# 'b'
# >>> 2+2
# 4
# ...
# >>> next(g)
# 8
# >>> next(g)
# Traceback (most recent call last):
#   File "<stdin>", line 1, in <module>
# StopIteration
