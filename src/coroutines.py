# декораторы
from datetime import datetime, time

# для поддержания парадигм принципа единой ответственности и DRY (don't repeat yourself) сделаем декоратор


def timeit(name=None):  # декоратор принимает в аргументы свои аргументы
    arg = name

    def outer(func):  # этой функции на вход отдается функция, над которой поставлен декоратор
        def wrapper(*args, **kwargs):  # функция в которой происходит вызов исходной функции с ее аргументами
            start = datetime.now()  # действие декоратора например
            result = func(*args, **kwargs)
            print(datetime.now() - start)
            return result
        return wrapper  # возвращается объект функции враппер, которая нужна для передачи аргументов исходной функции
    return outer  # декортатор возвращает объект функции outer, которая нужна для аргументов декоратора


@timeit(name="one")
def one(n):
    l = []
    for i in range(n):
        if i % 2 == 0:
            l.append(i)
    return l


@timeit(name="two")
def two(n):
    l = [x for x in range(n) if x % 2 == 0]


def test_dec():
    l1 = one(10)
    print(l1)
    # l2 = two(10**4)

    # скорость генератора выше
    # 0:00:00.000894
    # 0:00:00.000707

# Корутины - это подпрограммы, по сути генераторы, которые работают не только с next, но и с .send(), .throw()


def coroutine(func):  # декоратор необходим для того, чтобы запускать выполнение генератора сразу без g.send(None)/next(g)
    # !!! не определяет, что это корутина, это просто для удобства их запуска !!!
    def inner(*args, **kwargs):
        g = func(*args, **kwargs)
        g.send(None)
        return g
    return inner


def simplecor():
    x = "Ready to accept message"
    message = yield x  # в таком случае при остановке он вернет сообщение
    print("Simple cor recieved: ", message)


class SomeException(Exception):
    pass


@coroutine
def average():
    count = 0
    summ = 0
    average = None

    while True:
        try:
            x = yield average
        except StopIteration:  # Возможно передавать исключения в генератор g.throw(StopIteration)
            print('Done')
            break
        except SomeException:
            print('sss')
            break
        else:  # сделай если не исключение
            count += 1
            summ += x
            average = round(summ / count, 2)
    return average


def test_cor():
    g = average()
    print(g.send(5))
    print(g.send(6))
    try:
        g.throw(StopIteration)
    except StopIteration as e:
        print('Average', e.value)


test_cor()


# схема, объясняющая, что yield работает в две стороны
# g = gen() ---------------> next(g) / g.send(None) ---> mes = yield x ---------------> g.send("somemes"), mes = yield x ---------------> следующий yield или StopIteration
# создан, но не запущен      запущен                     приостановлен, вернул x        продолжает работу, mes принимает сообщение

# >>> g = subgen()

# В этой ситуации мы получим исключение, потому что выполнение требуется
# начинать со сдвига выполнения кода на yield
# >>> g.send("asdsd")
# Traceback (most recent call last):
#   File "<stdin>", line 1, in <module>
# TypeError: can't send non-None value to a just-started generator

# данная библиотека позволяет получить состояние генератора
# >>> from inspect import getgeneratorstate
# >>> getgeneratorstate(g)
# значит, что он создан, но не выполнялся до yield
# 'GEN_CREATED'

# данная конструкция позволяет сдвинуть выполнения кода на yield
# по сути мы передали ему пустое сообщение, что аналогично выполнению
# функции next(g) (можно использовать вместо этой конструкции).
# >>> g.send(None)

# значит, что его выполнение приостановлено (дошел до yield)
# >>> getgeneratorstate(g)
# 'GEN_SUSPENDED'

# Передадим в yield сообщение
# >>> g.send("asdsd")
# Subgen recieved:  asdsd # Выведено наше сообщение
# Traceback (most recent call last):
#   File "<stdin>", line 1, in <module>
# StopIteration
# таким образом генератор продолжил свою работу до следующего yield, которого он не нашел
