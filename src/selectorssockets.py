import socket
import selectors

selector = selectors.DefaultSelector()


def server():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # AF_INET: ipv4 SOCK_STREAM: tcp
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  # переиспользование адреса
    server_socket.bind(('localhost', 5000))  # адрес
    server_socket.listen()  # включение сокета
    selector.register(
        fileobj=server_socket,  # добавляем для мониторинга серверный сокет
        events=selectors.EVENT_READ,  # какое действие мониторить
        data=accept_connection  # связанные данные, по сути мы передаем вместе
        # с файлом сокета еще одимн объект и в данном случае этот объект будет определять функцию
        # которая должна вызываться для сокета
    )


def accept_connection(server_socket: socket):
    client_socket, addr = server_socket.accept()  # возвращает tuple(сокет клиента, адрес), блокирующая операция
    print('Connection from', addr)
    selector.register(  # регистрируем клиентский сокет
        fileobj=client_socket,
        events=selectors.EVENT_READ,
        data=send_message
    )


def send_message(client_socket: socket):
    request = client_socket.recv(4096)  # принимаем сообщение от сокета
    if request:  # если оно не пустое - отправляем сообщение
        response = 'Hello world\n'.encode()  # сообщение должно быть закодировано
        client_socket.send(response)  # если буфер отправки полон тоже блокирующая операция
    else:  # если сообщение пустое нужно закрыть соединение и убрать сокет из списка мониторинга
        selector.unregister(client_socket)  # сняли с регистрации клиентский сокет
        client_socket.close()  # закрыли клиентский сокет


def event_loop():
    while True:
        events = selector.select()  # (key, events) первый объект - это SelectorKey, который также содержит fileobj, events, data
        for key, _ in events:
            callback = key.data  # получаем функцию, которая была назначена для сокета
            callback(key.fileobj)  # вызываем эту функцию и в аргумент кладем сам объект сокета
            # таким образом для разных сокетов будет вызываться своя функция


if __name__ == "__main__":
    server()  # нужно инициировать сервер
    event_loop()
